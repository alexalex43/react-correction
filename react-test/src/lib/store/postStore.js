import { create } from 'zustand'

export const postStore = create((set) => ({
    posts: [],
    currentPost: null,
    setPosts: (posts) => set({posts}),
    setCurrentPost: (currentPost) => set({currentPost}),
    removePost: (id) =>{
      set((state) =>({
        posts: state.posts.filter((eleme)=> eleme.id !== id)
      }))
    }
  }))